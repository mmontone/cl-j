#
# Recommended settings for debuggin either mecl_min or mecl.
#
delete break
break cl_error
break CEerror
break FEerror
break FEunbound_variable
break FEundefined_function
break FEwrong_type_argument
break FEinvalid_function
break si_signal_simple_error
set confirm off
handle SIGPWR nostop
handle SIGXCPU nostop
handle SIGINT nostop pass
# our resume signal
handle SIG35 nostop pass
# our interrupt signal
handle SIG36 nostop pass



